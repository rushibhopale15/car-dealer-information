let findLastCar = require('./../problem2.cjs');
let inventory = require('./../main.cjs');
// we run the function findLastCar and store that information in lastCarInfo

let lastCarInfo = findLastCar(inventory);

if (lastCarInfo.length !== 0) {
    console.log("Last car is a " + lastCarInfo.car_make + " " + lastCarInfo.car_model);
}