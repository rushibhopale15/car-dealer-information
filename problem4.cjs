

function findCarYear(carArray){
     
    let carYears = [];      //store only years in caryears[] array and returns it            

    if(carArray === undefined  || !Array.isArray(carArray)){
        return carYears;
    }
    
        for(let index = 0; index < carArray.length; index++)     //finds car year oneByone to store the car_year
        {
        carYears.push(carArray[index].car_year);
        }
    return carYears;
}

module.exports = findCarYear;
