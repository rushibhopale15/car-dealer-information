let carArray = require('./main.cjs');


function findOnlyBMWAndAudi(carArray) {
    let bmwAndAudi = [];

    
    if (carArray === undefined || !Array.isArray(carArray)) {
        return bmwAndAudi;
    }
    for (let index = 0; index < carArray.length; index++) {
        if (carArray[index].car_make === "BMW" || carArray[index].car_make === "Audi") {
            bmwAndAudi.push(carArray[index]);
        }
    }
    return bmwAndAudi;
}


module.exports = findOnlyBMWAndAudi;