


function findOlderCar(carYears) {

    let oldCarthan2000 = [];
    if (carYears === undefined || !Array.isArray(carYears)) {
        return oldCarthan2000;
    }

    for (let index = 0; index < carYears.length; index++) {
        if (carYears[index] < 2000) {
            oldCarthan2000.push(carYears[index]);
        }
    }
    return oldCarthan2000;
}

module.exports = findOlderCar;
