
//function to store car_model_names
function findCarModelNames(carArray) {

    let carModelNames = [];
    for (let x = 0; x < carArray.length; x++) {
        carModelNames.push(carArray[x].car_model)
    }
    return carModelNames;
}


//print the car_model_names is asending order
function printCarModelNames(carArray) {
    let carModelNames = [];
    if (carArray === undefined || !Array.isArray(carArray)) {
        return carModelNames;
    }
    carModelNames = findCarModelNames(carArray);
    carModelNames.sort();
    return carModelNames;
}

module.exports = printCarModelNames;
