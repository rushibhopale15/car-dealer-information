

function findOutCar(carArray, id)
{
    let carInfo = [];

    if(carArray === undefined  || id === undefined || id === null || typeof id !== 'number' || !Array.isArray(carArray)){
        return carInfo;
    }
    
    
    for(let index = 0; index <carArray.length; index++)
    {
       if(id === carArray[index].id ) 
       {
        carInfo= carArray[index] ;
        return carInfo;
       }
    }
    return carInfo;
}

module.exports = findOutCar;

