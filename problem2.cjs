// let carArray = require('./main.cjs');

function findLastCar(carArray) {
    let carInfo = [];

    if (carArray === undefined || !Array.isArray(carArray)) {
        return carInfo;
    }
    carInfo = carArray[carArray.length - 1];
    return carInfo;
}

module.exports = findLastCar;